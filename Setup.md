## Setup Backend Code

The instructions will be divided into parts. 

### Inital

- Go to Src/symfony_docker/app
- Open .env and .env.test
- You Can get  the following params ( MYSQL_ROOT_PASSWORD & MYSQL_USER & MYSQL_PASSWORD & MYSQL_DATABASE)

## Start Project
From the base directory run these steps:

1. In terminal execute this command( make start-project )
2. In terminal execute this command .(make composer-install) 
3. In new terminal execute this command (make database-inital) .
4. Connect to mysql and create DB
5. Put the name of db inside .env to parameter (MYSQL_DATABASE) 
6. In terminal execute this command (make database-update)
7. After the database is updated you can execute ( make fixture-inital-project )
8. Your application will be run on (http://localhost:8080/)
9. Run rabbitmq you can execute ( make run-enqueue)


  
### Bundles Used
1. EnqueueBundle: (https://php-enqueue.github.io/bundle/quick_tour/)
2. Guzzle http client: (https://docs.guzzlephp.org/en/stable/)

## Ideas that Used
* Make custom annotation like route in symfony 
* the custom annotation that used GitManagement
* you can find this annontation class in src/app/symfony_docker/src/GitManagements/Annotation/GitManagement

 
## Entities
 
1. I created GitType entity that can have many types like ( github & bitbucket)
2. I created Owner Entity that can have many owners like ( nodejs )
3. I created Project Entity that can have many projects like ( node )
4. if you create a new entity you can update schema with this command ( make database-update)

## Tests
1. To run Unit Test execute this command (make phpunit)
2. To run behat Test execute this command in terminal (make behat)

## Other Notes
1. I make seralizer and validation groups in entity to tell the reviewer that i can have experience on it and if the reviewer want to complete it with apis (seralizer) or use validation groups i will do it 
2. I pushed .env and .env.test to see you what i did it in these files 