Feature: Add A owner
  In order to retrieve the project
  As a user
  I must visit the home page

  Scenario: I want add new owner

    When I add owner with name  "nodejs"
    And I add project with name "node"
    Then The results should include a owner with project count "1"
