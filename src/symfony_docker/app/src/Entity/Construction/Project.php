<?php

namespace App\Entity\Construction;

use App\Entity\Commit\Commit;
use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="projects")
 */
class Project implements EntityInterface
{
    private $id;
    /**
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $name;

    /**
     * @Assert\Valid(groups={"create"})
     *
     */
    private $owner;

    /**
     * @var \DateTimeInterface|null
     */
    protected $createdAt;
    /**
     * @return mixed
     */
    public function __construct()
    {

        $this->projects = new ArrayCollection();
    }
    public function getId():int
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    /**
     * @return Owner
     */
    public function getOwner(): ?Owner
    {
        return $this->owner;
    }
    /**
     * @return ArrayCollection|Commit[]
     */
    public function getCommits(): Collection
    {
        return $this->commits;
    }
    public function addCommit(Commit $commit): self
    {
        if (!$this->commits->contains($commit))
        {
            $this->commits[] = $commit;
            $commit->setAuthor($this);
        }
        return $this;
    }
    public function removeCommit(Commit $commit): self
    {
        if ($this->commits->contains($commit))
        {
            $this->commits->removeElement($commit);
            if ($commit->getAuthor() === $this)
            {
                $commit->setAuthor(null);
            }
        }
        return $this;
    }
    public function setOwner(?Owner $owner): self
    {
        $this->owner = $owner;
        return $this;
    }
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
    public function setCreatedAt(?\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
