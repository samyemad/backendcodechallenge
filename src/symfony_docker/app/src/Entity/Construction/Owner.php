<?php

namespace App\Entity\Construction;

use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="owners")
 */
class Owner implements EntityInterface
{
    private $id;
    /**
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $name;
    /**
     * @Assert\Valid(groups={"create"})
     *
     */
    private $gitType;
    /**
     * @Assert\Valid(groups={"create"})
     * @Groups({"group1"})
     */
    private $projects;
    /**
     * @var \DateTimeInterface|null
     */
    protected $createdAt;
    /**
     * @return mixed
     */
    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }
    public function getId():int
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    /**
     * @return ArrayCollection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }
    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project))
        {
            $this->projects[] = $project;
            $project->setOwner($this);
        }
        return $this;
    }
    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project))
        {
            $this->projects->removeElement($project);
            if ($project->getOwner() === $this)
            {
                $project->setOwner(null);
            }
        }
        return $this;
    }
    /**
     * @return GitType
     */
    public function getGitType(): ?GitType
    {
        return $this->gitType;
    }
    public function setGitType(?GitType $gitType): self
    {
        $this->gitType = $gitType;
        return $this;
    }
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
    public function setCreatedAt(?\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
