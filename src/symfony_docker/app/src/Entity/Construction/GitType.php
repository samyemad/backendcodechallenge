<?php

namespace App\Entity\Construction;

use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="git_types")
 */
class GitType implements EntityInterface
{
    private $id;
    /**
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $name;
    /**
     * @Assert\Valid(groups={"create"})
     * @Groups({"group1"})
     */
    private $owners;

    /**
     * @var \DateTimeInterface|null
     */
    protected $createdAt;
    /**
     * @return mixed
     */
    public function __construct()
    {

        $this->owners = new ArrayCollection();
    }
    public function getId():int
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    /**
     * @return ArrayCollection|Owner[]
     */
    public function getOwners(): Collection
    {
        return $this->owners;
    }
    public function addOwner(Owner $owner): self
    {
        if (!$this->owners->contains($owner))
        {
            $this->owners[] = $owner;
            $owner->setGitType($this);
        }
        return $this;
    }
    public function removeOwner(Owner $owner): self
    {
        if ($this->owners->contains($owner))
        {
            $this->owners->removeElement($owner);
            if ($owner->getGitType() === $this)
            {
                $owner->setGitType(null);
            }
        }
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
