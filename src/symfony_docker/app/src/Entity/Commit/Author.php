<?php

namespace App\Entity\Commit;

use App\Entity\Commit\Commit;
use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="authors")
 */
class Author implements EntityInterface
{
    private $id;
    /**
     * @var mixed
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $name;
    /**
     * @var string
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $email;
    /**
     * @var Collection|Commit[]
     * @Assert\Valid(groups={"create"})
     * @Groups({"group1"})
     */
    private $commits;
    /**
     * @var \DateTimeInterface|null
     */
    protected $createdAt;
    /**
     * @return mixed
     */
    public function __construct()
    {
        $this->commits = new ArrayCollection();
    }
    public function getId():int
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }
    /**
     * @param string $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
    /**
     * @return ArrayCollection|Commit[]
     */
    public function getCommits(): Collection
    {
        return $this->commits;
    }
    public function addCommit(Commit $commit): self
    {
        if (!$this->commits->contains($commit))
        {
            $this->commits[] = $commit;
            $commit->setAuthor($this);
        }
        return $this;
    }
    public function removeCommit(Commit $commit): self
    {
        if ($this->commits->contains($commit))
        {
            $this->commits->removeElement($commit);
            if ($commit->getAuthor() === $this)
            {
                $commit->setAuthor(null);
            }
        }
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function serialize()
    {
        return serialize(array($this->email));
    }
}
