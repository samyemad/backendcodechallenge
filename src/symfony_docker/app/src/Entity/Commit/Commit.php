<?php

namespace App\Entity\Commit;

use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Construction\Project;
use App\Entity\Commit\Author;

/**
 * @ORM\Entity
 * @ORM\Table(name="commits")
 */
class Commit implements EntityInterface
{
    private $id;
    /**
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $message;
    /**
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $hash;
    /**
     * @Assert\NotBlank(groups={"create"})
     * @Groups({"group1"})
     */
    private $node;
    /**
     * @Assert\Valid(groups={"create"})
     *
     */
    private $author;
    /**
     * @Assert\Valid(groups={"create"})
     *
     */
    private $project;
    /**
     * @var \DateTimeInterface|null
     */
    protected $createdAt;
    /**
     * @return mixed
     */
    public function getId():int
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }
    /**
     * @return mixed
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }
    /**
     * @param mixed $hash
     */
    public function setHash($hash): void
    {
        $this->hash = $hash;
    }
    /**
     * @return mixed
     */
    public function getNode(): ?string
    {
        return $this->node;
    }
    /**
     * @param mixed $node
     */
    public function setNode($node): void
    {
        $this->node = $node;
    }
    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }
    public function setProject(?Project $project): self
    {
        $this->project = $project;
        return $this;
    }
    /**
     * @return Author
     */
    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
