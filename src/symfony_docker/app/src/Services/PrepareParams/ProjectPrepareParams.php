<?php

namespace App\Services\PrepareParams;

use App\Entity\EntityInterface;
use App\Services\PrepareParams\PrepareParamsInterface;

class ProjectPrepareParams implements PrepareParamsInterface
{
    /**
     * Prepare Params that has been sent to Git Management to execute the run function
     * @param EntityInterface $entity
     * @return array
     */
    public function run(EntityInterface $entity): array
    {
        $parameters['size']=100;
        $parameters['project']=$entity->getName();
        if($entity->getOwner() != null)
        {
            $parameters['owner'] = $entity->getOwner()->getName();
        }
        if($entity->getOwner()->getGitType() != null)
        {
            $parameters['git_type']=$entity->getOwner()->getGitType()->getName();
        }
        return $parameters;

    }

}
