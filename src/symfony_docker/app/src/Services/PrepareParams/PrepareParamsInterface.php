<?php

namespace App\Services\PrepareParams;

use App\Entity\EntityInterface;

interface PrepareParamsInterface
{
    public function run(EntityInterface $entity): array;
}
