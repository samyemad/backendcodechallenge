<?php

namespace App\Services\Event;

use App\Services\Event\GenerateEventInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Entity\Construction\Project;

class GenerateEvent implements GenerateEventInterface
{
    /**
     * Generate Event and dispatch the event to make listeners notify this event
     * @param string $value
     * @param Array $rows
     * @param Project $project
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function generate(string $value,Array $rows,Project $project,EventDispatcherInterface $eventDispatcher): void
    {
        $event = new $value($rows,$project);
        $eventDispatcher->dispatch($event, $value::NAME);
    }
}

