<?php

namespace App\Services\Event;

use App\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Entity\Construction\Project;

interface GenerateEventInterface
{
    public function generate(string $value,Array $rows,Project $project,EventDispatcherInterface $eventDispatcher): void;
}
