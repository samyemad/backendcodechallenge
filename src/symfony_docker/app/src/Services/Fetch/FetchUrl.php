<?php

namespace App\Services\Fetch;

use App\Services\Fetch\FetchUrlInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use GuzzleHttp\Client;

class FetchUrl implements FetchUrlInterface
{
    /**
     * Generate Base URL And Fetch the Data from URL
     * @param string $baseUrl
     * @param string $uri
     * @param array $headers
     * @param array $body
     * @param string $method
     * @return array
     */
    public function generate(string $baseUrl,string $uri,array $headers,array $body,string $method = 'get'): array
    {
        $rows=[];
        $baseInfo=$this->prepareBaseInfo($baseUrl,$headers);
        $client = new Client($baseInfo);
        $rowsBody=[];
        if(!empty($body))
        {
            $rowsBody['body'] = json_encode($body);
        }
        $response = $client->$method($uri, $rowsBody);

        if($response->getStatusCode() == 200)
        {
            $rows = json_decode($response->getBody()->getContents(), true);
        }
        return $rows;
    }
    /**
     * Prepare Base Info to guzzle http client
     * @param string $baseUrl
     * @param array $headers
     * @return array
     */
    private function prepareBaseInfo(string $baseUrl,array $headers): array
    {
        return [
            'base_uri' => $baseUrl,
            'http_errors' => false,
            'defaults' => [
                'exceptions' => false
            ],
            'headers' => $headers
        ];
    }
}

