<?php

namespace App\Services\Fetch;

use App\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

interface FetchUrlInterface
{
    public function generate(string $baseUrl,string $uri,array $headers,array $body,string $method): array;

}
