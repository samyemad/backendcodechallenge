<?php

namespace App\Services\Manager\Author;

use App\Entity\Commit\Author;

interface SaveAuthorInterface
{
    public function run(array $author): Author;
}
