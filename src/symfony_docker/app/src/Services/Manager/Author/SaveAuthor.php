<?php

namespace App\Services\Manager\Author;

use App\Entity\Commit\Author;
use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class SaveAuthor implements SaveAuthorInterface
{
    public function __construct(EntityManagerInterface $em)
    {
      $this->em = $em;
    }
    /**
     * Save Author Data to the Author Entity
     * @param array $author
     * @return Author
     */
    public function run(array $author): Author
    {
        $checkAuthor=$this->em->getRepository(Author::class)->findOneBy(['email' => $author['email']]);
        if($checkAuthor == null)
        {
            $newAuthor = new Author();
            $newAuthor->setEmail($author['email']);
            $newAuthor->setName($author['name']);
            $unPersistedAuthor=$this->checkPresistedAuthor($newAuthor);
            if(!$unPersistedAuthor) {
                $this->em->persist($newAuthor);
                return $newAuthor;
            }
            else
            {
                return $unPersistedAuthor;
            }
        }
        return $checkAuthor;

    }
    /**
     * Check Author Duplicate before flushing the manager
     * @param Author $newAuthor
     * @return mixed
     */
    private function checkPresistedAuthor($newAuthor)
    {
        $uow = $this->em->getUnitOfWork();
        $insertions = $uow->getScheduledEntityInsertions();
        $insertions = new ArrayCollection($insertions);
        $unPersistedAuthor = $insertions->filter(
            function(EntityInterface $model) use ($newAuthor)
            {
                if($model instanceof  Author)
                {
                    return $model->serialize() === $newAuthor->serialize();
                }
            }
        )->first();
        return $unPersistedAuthor;
    }
}
