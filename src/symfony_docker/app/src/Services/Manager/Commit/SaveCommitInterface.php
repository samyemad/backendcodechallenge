<?php

namespace App\Services\Manager\Commit;

use App\Entity\Commit\Author;
use App\Entity\Construction\Project;

interface SaveCommitInterface
{
    public function run(Author $author,array $row,Project $project): void;
}
