<?php

namespace App\Services\Manager\Commit;

use App\Entity\Commit\Author;
use App\Entity\Commit\Commit;
use App\Entity\Construction\Project;
use App\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class SaveCommit implements SaveCommitInterface
{
    public function __construct(EntityManagerInterface $em)
    {
      $this->em = $em;
    }
    /**
     * Save Commit Data to the Commit Entity
     * @param Author $author
     * @param array $row
     * @param Project $project
     */
    public function run(Author $author,array $row,Project $project): void
    {
        $checkCommit=$this->em->getRepository(Commit::class)->findOneBy(['hash' => $row['sha']]);
        if($checkCommit == null)
        {
            $dateTime = new \DateTime('NOW');
            $newCommit = new Commit();
            $newCommit->setHash($row['sha']);
            $newCommit->setMessage($row['commit']['message']);
            $newCommit->setAuthor($author);
            $newCommit->setProject($project);
            $newCommit->setNode($row['node_id']);
            $newCommit->setMessage($row['commit']['message']);
            $newCommit->setCreatedAt($dateTime);
            $this->em->persist($newCommit);
        }

    }

}
