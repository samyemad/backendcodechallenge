<?php

namespace App\Services\PrepareUrl;

use App\Services\PrepareUrl\PrepareUrlInterface;
use App\Services\Fetch\FetchUrlInterface;

class GithubPrepareUrl implements PrepareUrlInterface
{
    const GITHUBURL='https://api.github.com';
    protected $fetchUrl;
    public function __construct(FetchUrlInterface $fetchUrl)
    {
      $this->fetchUrl = $fetchUrl;
    }
    /**
     * Prepare URL that has been sent to Fetch Url  to execute the generate function
     * @param array $parameters
     * @return array
     */
    public function run(array $parameters): array
    {
        if($parameters['owner'] != null && $parameters['project'] != null)
        {
           $prepareUrl=$this->prepareUri($parameters['owner'],$parameters['project']);
        }
        if($parameters['page'] != null && $parameters['size'] != null)
        {
            $prepareUrl.=$this->prepareQueryParameters($parameters['page'],$parameters['size']);
        }
        $rows=$this->fetchUrl->generate(self::GITHUBURL,$prepareUrl,$this->getHeaders(),$this->getBody());
        return $rows;
    }
    /**
     * Prepare URI that has been sent to generate function in fetch url class
     * @param string $owner
     * @param string $project
     * @return string
     */
    public function prepareUri(string $owner,string $project): string
    {
      return '/repos/'.$owner.'/'.$project.'/commits';
    }
    /**
     * Prepare QueryParameters that has been sent to generate function in fetch url class
     * @param string $page
     * @param string $size
     * @return string
     */
    public function prepareQueryParameters(string $page,string $size): string
    {
      return "?per_page=".$size."&page=".$page."&order=desc";
    }
    /**
     * Prepare Headers that has been sent to generate function in fetch url class
     * @return array
     */
    public function getHeaders(): array
    {
        return ['Content-Type' => 'application/json'];
    }
    /**
     * Prepare Body that has been sent to generate function in fetch url class
     * @return array
     */
    public function getBody(): array
    {
        return [];
    }
}