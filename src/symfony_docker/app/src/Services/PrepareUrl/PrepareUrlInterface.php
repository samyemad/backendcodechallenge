<?php

namespace App\Services\PrepareUrl;

interface PrepareUrlInterface
{
    public function run(array $parameters): array;

    public function prepareUri(string $owner,string $project): string;

    public function prepareQueryParameters(string $page,string $size): string;

    public function getHeaders(): array;

    public function getBody(): array;
}
