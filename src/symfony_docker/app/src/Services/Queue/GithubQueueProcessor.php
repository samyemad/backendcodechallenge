<?php

namespace App\Services\Queue;

use App\Event\CommitInsertedEvent;
use Interop\Queue\Message;
use Interop\Queue\Context;
use Interop\Queue\Processor;
use Enqueue\Client\TopicSubscriberInterface;
use App\Services\Event\GenerateEventInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Services\GitHub\PrepareUrlProcessor;
use App\Services\PrepareUrl\PrepareUrlInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Construction\Project;

class GithubQueueProcessor implements Processor, TopicSubscriberInterface
{
    /**
     * @var GenerateEventInterface
     */
    protected $customEvent;
    /**
     * @var EventDispatcherInterface
     */
    protected  $eventDispatcher;
    /**
     * @var PrepareUrlInterface
     */
    protected $prepareUrlProcessor;
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(GenerateEventInterface $generateEvent,EventDispatcherInterface $eventDispatcher,PrepareUrlInterface $githubPrepareUrl,EntityManagerInterface $em)
    {
      $this->customEvent = $generateEvent;
      $this->eventDispatcher =  $eventDispatcher;
      $this->prepareUrlProcessor = $githubPrepareUrl;
      $this->em = $em;
    }
    /**
     * Process the rabbitmq event and consuming messages
     * @param Message $message
     * @param Context $session
     * @return void
     */
    public function process(Message $message, Context $session)
    {
        $project=$this->em->getRepository(Project::class)->findOneBy(['name' => $message->getProperty('project')]);
        if($project == null)
        {
            return self::REJECT;
        }
       $rows=$this->prepareUrlProcessor->run($message->getProperties());
       $this->customEvent->generate(CommitInsertedEvent::class,$rows,$project,$this->eventDispatcher);
        return self::ACK;
    }

    public static function getSubscribedTopics()
    {
        return ['GitHubTopic'];
    }
}
