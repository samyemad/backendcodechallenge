<?php

namespace App\EventListener;

use App\Event\CommitInsertedEvent;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\Manager\Author\SaveAuthorInterface;
use App\Services\Manager\Commit\SaveCommitInterface;

class CommitInsertedNotifier
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var SaveAuthorInterface */
    private $saveAuthor;
    /** @var SaveCommitInterface */
    private $saveCommit;
    public function __construct(EntityManagerInterface $em, SaveAuthorInterface $saveAuthor,SaveCommitInterface $saveCommit)
    {
     $this->em = $em;
     $this->saveAuthor = $saveAuthor;
     $this->saveCommit =  $saveCommit;
    }
    /**
     * Notify the commit entity and author entity about new data that comes from git api
     * @param CommitInsertedEvent $event
     */
    public function notify(CommitInsertedEvent $event): void
    {
       $rows=$event->fetch();
       $project=$event->getProject();
       foreach ($rows as $key => $row)
       {
           $selectedAuthor=null;
           if($row['commit'] != null)
           {
            if(is_array($row['commit']['author']))
            {
                $selectedAuthor=$this->saveAuthor->run($row['commit']['author']);
            }
            if($row['sha'] != null)
            {
                $this->saveCommit->run($selectedAuthor,$row,$project);
            }
           }
       }
       $this->em->flush();
    }
}

