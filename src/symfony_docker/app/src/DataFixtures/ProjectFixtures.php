<?php

namespace App\DataFixtures;

use App\Entity\Construction\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use App\Entity\Construction\GitType;
use App\Entity\Construction\Owner;

class ProjectFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $dateTime = new \DateTime('NOW');
        $gitType=new GitType();
        $gitType->setName('GitHub');
        $gitType->setCreatedAt($dateTime);
        $owner=new Owner();
        $owner->setName('nodejs');
        $owner->setCreatedAt($dateTime);
        $project = new Project();
        $project->setName('node');
        $project->setCreatedAt($dateTime);
        $owner->addProject($project);
        $gitType->addOwner($owner);
        $manager->persist($gitType);
        $manager->flush();
    }


     public static function getGroups(): array
     {
         return ['projects'];
     }
}
