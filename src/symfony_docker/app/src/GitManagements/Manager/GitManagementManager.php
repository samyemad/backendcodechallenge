<?php

namespace App\GitManagements\Manager;

use App\GitManagements\Discovery\GitManagementDiscoveryInterface;

class GitManagementManager implements GitManagementManagerInterface
{
    /**
     * @var GitManagementDiscoveryInterface
     */
    private $discovery;

    public function __construct(GitManagementDiscoveryInterface $gitManagementDiscovery)
    {
        $this->discovery = $gitManagementDiscovery;
    }
    /**
     * Returns a list of available GitManagements.
     *
     * @return array
     */
    public function getGitManagements()
    {
        return $this->discovery->getGitManagements();
    }
    /**
     * Returns one gitManagement by name
     *
     * @param $name
     * @return array
     * @throws \Exception
     */
    public function getGitManagement($name)
    {
        $gitManagements = $this->discovery->getGitManagements();
        if (isset($gitManagements[$name]))
        {
            return $gitManagements[$name];
        }
        throw new \Exception('GitManagement not found.');
    }
    /**
     * Creates a gitManagement
     * @param $name
     * @return GitManagementInterface
     * @throws \Exception
     */
    public function create($name)
    {
        $gitManagements = $this->discovery->getGitManagements();
        if (array_key_exists($name, $gitManagements))
        {
            $class = $gitManagements[$name]['class'];
            if (!class_exists($class))
            {
                throw new \Exception('GitManagement class does not exist.');
            }
            return new $class();
        }
        throw new \Exception('GitManagement does not exist.');
    }
}