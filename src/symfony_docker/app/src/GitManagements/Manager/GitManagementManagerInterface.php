<?php
namespace App\GitManagements\Manager;

use Symfony\Component\Finder\SplFileInfo;

interface GitManagementManagerInterface
{
    /**
     * Returns a list of available GitManagements.
     * @return array
     */
    public function getGitManagements();
    /**
     * Returns one gitManagement by name
     * @param $name
     * @return arrays
     * @throws \Exception
     */
    public function getGitManagement($name);
    /**
     * Creates a gitManagement
     * @param $name
     * @return GitManagementInterface
     * @throws \Exception
     */
    public function create($name);
}