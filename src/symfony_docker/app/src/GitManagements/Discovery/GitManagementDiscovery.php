<?php

namespace App\GitManagements\Discovery;

use App\GitManagements\Annotation\GitManagement;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpKernel\Config\FileLocator;

class GitManagementDiscovery implements GitManagementDiscoveryInterface
{
    /**
     * @var string
     */
    private $namespace;
    /**
     * @var string
     */
    private $directory;
    /**
     * @var Reader
     */
    private $annotationReader;

    /**
     * The Kernel root directory
     * @var string
     */
    private $rootDir;
    /**
     * The Annontation Class
     * @var mixed
     */
    private $annontationClass;
    /**
     * @var array
     */
    private $gitManagements = [];
    /**
     * GitManagementDiscovery constructor.
     * @param $namespace
     *   The namespace of the gitManagements
     * @param $directory
     *   The directory of the gitManagements
     * @param $rootDir
     * @param Reader $annotationReader
     */
    public function __construct($namespace, $directory,$annontationClass, $rootDir, Reader $annotationReader)
    {
        $this->namespace = $namespace;
        $this->annotationReader = $annotationReader;
        $this->directory = $directory;
        $this->rootDir = $rootDir;
        $this->annontationClass= $annontationClass;
    }
    /**
     * Returns all the gitManagements
     */
    public function getGitManagements(): array
    {
        if (!$this->gitManagements)
        {
            $this->discoverGitManagements();
        }
        return $this->gitManagements;
    }
    /**
     * Discovers gitManagements
     */
    private function discoverGitManagements()
    {
        $path = $this->rootDir . '/src/' . $this->directory.'/Types';
        $finder = new Finder();
        $finder->files()->in($path);
        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $class = $this->namespace . '\\Types\\' . $file->getBasename('.php');
            if($file->getBasename('.php')  != $this->annontationClass)
            {
                $annotation = $this->annotationReader->getClassAnnotation(new \ReflectionClass($class), GitManagement::class);
                if (!$annotation)
                {
                    continue;
                }
                /** @var GitManagement $annotation */
                $this->gitManagements[$annotation->getName()] = [
                    'class' => $class,
                    'annotation' => $annotation,
                ];
            }
        }
    }
}