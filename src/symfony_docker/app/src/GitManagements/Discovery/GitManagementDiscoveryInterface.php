<?php

namespace App\GitManagements\Discovery;

use Symfony\Component\Finder\SplFileInfo;

interface GitManagementDiscoveryInterface
{
    /**
     * Returns a list of available GitManagements.
     *
     * @return array
     */
    public function getGitManagements():array;
}