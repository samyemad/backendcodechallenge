<?php
namespace App\GitManagements\Types;

use Symfony\Component\Finder\SplFileInfo;
use Enqueue\Client\ProducerInterface;

interface GitManagementInterface
{
    /**
     * Does the gitManagement
     *
     * @return NULL
     */
    public function run(ProducerInterface $producer,array $parameters);
}