<?php

namespace App\GitManagements\Types;

use App\GitManagements\Annotation\GitManagement;
use App\GitManagements\Types\GitManagementInterface;
use Enqueue\Client\Message;
use Enqueue\Client\ProducerInterface;
/**
 * Class GitHub
 * @GitManagement(
 *     name = "GitHub",
 * )
 */
class GitHub implements GitManagementInterface
{
    /**
     * Run the GITHUB API with enqueue consumer by sending event to github queue processor file
     * @param ProducerInterface $producer
     * @param array $parameters
     */
    public function run(ProducerInterface $producer,array $parameters)
    {
        $headers=[];
        $message = new Message('Added Commits', $parameters, $headers);
        $producer->sendEvent('GitHubTopic', $message);
    }
}