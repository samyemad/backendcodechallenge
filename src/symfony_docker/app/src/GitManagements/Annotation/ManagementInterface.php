<?php

namespace App\GitManagements\Annotation;

use Symfony\Component\Finder\SplFileInfo;

interface ManagementInterface
{
    /**
     * Returns the name of specified Management.
     *
     * @return array
     */
    public function getName():string;

}