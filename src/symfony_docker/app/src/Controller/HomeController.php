<?php

namespace App\Controller;

use App\Entity\Commit\Commit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Enqueue\Client\ProducerInterface;
use App\GitManagements\Manager\GitManagementManagerInterface;
use App\Form\Author\SearchByAuthorType;
use Symfony\Component\HttpFoundation\Request;
use App\Form\Project\SearchByProjectType;
use App\Services\PrepareParams\PrepareParamsInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ProducerInterface $producer,GitManagementManagerInterface $gitManagementManager,Request $request,PrepareParamsInterface $projectPrepareParams): Response
    {

        $form=$this->createForm(SearchByProjectType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $data=$form->getData();
            if(is_array($data))
            {
                $project=$data['project'];
                if($project != null)
                {
                    $parameters=$projectPrepareParams->run($project);
                    for($i=1;$i<=10;$i++)
                    {
                        $parameters['page']=$i;
                        $gitManagement = $gitManagementManager->create($parameters['git_type']);
                        $gitManagement->run($producer, $parameters);
                    }
                    $this->addFlash('success', 'PLease wait Until all commits inserted in our system you can check on it for while ');

                }
            }
        }
        return $this->render('commit/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/search", name="search_author")
     */
    public function search(Request $request)
    {
        $selectedCommits=[];
        $em=$this->getDoctrine()->getManager();
        $form=$this->createForm(SearchByAuthorType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            if(is_array($form->getData()))
            {
                $data=$form->getData();
                $author=$data['author'];
                $selectedCommits=$em->getRepository(Commit::class)->findBy(['author' => $author]);
            }
        }
        return $this->render('commit/search.html.twig', [
            'form' => $form->createView(),
            'commits' => $selectedCommits
        ]);
    }
}
