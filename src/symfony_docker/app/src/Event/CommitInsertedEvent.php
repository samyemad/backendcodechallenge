<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Construction\Project;

class CommitInsertedEvent extends Event
{
    public const NAME = 'app.commit.inserted';

    protected $rows;

    protected $project;

    public function __construct(Array $rows,Project $project)
    {
        $this->rows = $rows;
        $this->project = $project;
    }

    public function fetch(): Array
    {
        return $this->rows;
    }

    public function getProject(): Project
    {
        return $this->project;
    }
}

