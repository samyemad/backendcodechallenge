<?php
// tests/Service/NewsletterGeneratorTest.php
namespace App\Tests\Requests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
class CommitsTest extends KernelTestCase
{
    public function testAuthorizeApi()
    {

        // (1) boot the Symfony kernel
        $kernel=self::bootKernel();
        $headers = [
            'Content-Type' => 'application/json',
        ];
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://api.github.com',
            'debug'=> true,
            'defaults' => [
                'exceptions' => false
            ],
            'headers' => $headers
        ]);
        $response = $client->get('repos/nodejs/node/commits?per_page=100&page=2&order=desc',
            []
        );
        $content=json_decode($response->getBody()->getContents(),true);
       $this->assertEquals(200,$response->getStatusCode());
       $this->assertIsArray($content);



    }
}
