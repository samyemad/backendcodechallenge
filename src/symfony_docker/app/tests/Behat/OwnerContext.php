<?php
namespace App\Tests\Behat;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert as Assertions;
use App\Entity\Construction\Owner;
use App\Entity\Construction\Project;

/**
 * Defines application features from the specific context.
 */
class OwnerContext implements Context
{

    private $owner;

    private $path;

    private $content;
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When I add owner with name  :arg1
     */
    public function iAddOwnerWithName($arg1)
    {
        $owner= new Owner();
        $owner->setName($arg1);
        $this->owner=$owner;
        Assertions::assertNotNull($owner);
    }

    /**
     * @When I add project with name :arg1
     */
    public function iAddProjectWithName($arg1)
    {
        $project = new Project();
        $project->setName($arg1);

        $this->owner->addProject($project);
        Assertions::assertNotNull($project);
    }

    /**
     * @Then The results should include a owner with project count :arg1
     */
    public function theResultsShouldIncludeOwnerWithProjectCount($arg1)
    {
        $projects=$this->owner->getProjects();
        Assertions::assertEquals(1,count($projects));
    }
}
