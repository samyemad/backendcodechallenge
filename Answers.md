## Answers 

The Answers will be divided into parts. 

### How were you debugging this mini-project? Which tools?

- use debug component in symfony (https://symfony.com/doc/4.1/components/debug.html)

- I enable the error handler and enable the exception handler
- I use service container linter in symfony to check errors and find it  (https://symfony.com/blog/new-in-symfony-4-4-service-container-linter)
- I use phpstan for finding errors in my code ( https://symfony.com/projects/phpstan)
- 

## How were you testing the mini-project?
From the base directory run these steps:

1. integration test that symfony provide to us by first boot the kernel and then can make any integration test
2- using behat and i used behat feature on this project


  
### Imagine this mini-project needs microservices with one single database, how would you draft an architecture?
1.![microservices](https://i.postimg.cc/52sFds7y/1-micro.jpg)
2.![sharding](https://i.postimg.cc/ZqsngXMK/sharding.jpg)
3- make too fine grained services depend on the GIT type and then make
each of service connect to specific shard that contain only the sepecific data for git type
I mean projects and owners and commits and authors related to git type put in the different shard
4- I used light weight message broker to only access remote service components we have it 


 
## How would your solution differ when all over the sudden instead of saving to a Database you would have to call another external API to store and receive the commits.??
 
1. ![microservices2](https://i.postimg.cc/wxJ6B3X6/2-micro.jpg)
2. if we have only one external api then we can use this design to only access the api layer 
3. if we have multiple external services then we can use saga pattern for each internal service with specific external service as one component 
4. i mean saga pattern-orchestration between one internal service and many external services 
5. for example we have order service and access many services like ( inventory-payment-shipping)  when we make order
6. the order service fetch data from inventory service and then from shipping and then access payment service to get payment methods and put payments
 